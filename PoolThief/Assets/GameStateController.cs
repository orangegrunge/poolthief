﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameStateController : MonoBehaviour
{
    GameStateUI stateUI;

    List<BallController> ballControllers;
    List<List<Vector3>> posHistroy;
    List<List<Vector3>> rotHistroy;


    public int ballTotal;
    public int ballLeft;
    public int turn = 1;

    private void Start()
    {
        FindStateUI();
    }

    public void InitBall(BallController ball)
    {
        ballTotal++;
        ballLeft++;
        UpdateUI();
    }

    public void RemoveBall(BallController ball)
    {
        ballLeft--;
        UpdateUI();
    }

    private void UpdateUI()
    {
        FindStateUI();
        stateUI.BallTotal = ballTotal;
        stateUI.BallLeft = ballLeft;
    }

    private void FindStateUI()
    {
        if (!stateUI) stateUI = FindObjectOfType<GameStateUI>();
    }
}
