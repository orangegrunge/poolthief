﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class GameStateUI : MonoBehaviour
{
    [SerializeField] Text ballTotal;
    [SerializeField] Text ballLeft;
    [SerializeField] Text turn;

    public int BallTotal { set => ballTotal.text = "Ball Total: " + value.ToString(); }
    public int BallLeft { set => ballLeft.text = "Ball Left: " + value.ToString(); }
    public int Turn { set => turn.text = value.ToString(); }
}
