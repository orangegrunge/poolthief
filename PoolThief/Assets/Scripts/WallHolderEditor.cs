﻿using System.Collections;
using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using UnityEngine.Tilemaps;




[CustomEditor(typeof(WallHolder))]
public class WallHolderEditor : Editor
{
    [SerializeField] WallHolder script;
    [SerializeField] public List<Tile> tiles;
    bool pointTogglesLocked = false;

    Vector2 scrollPos;


    private void OnEnable()
    {
        script = (WallHolder)target;
    }

    public override void OnInspectorGUI()
    {
        if (GUILayout.Button("Refresh Tile List")) script.UpdateTileList();

        pointTogglesLocked = GUILayout.Toggle(pointTogglesLocked, "Lock Point Toggles");

        using (var s0 = new EditorGUILayout.ScrollViewScope(scrollPos, GUILayout.Height(500)))
        {
            scrollPos = s0.scrollPosition;
            foreach (WallTileSettings tile in script.tiles)
            {
                DrawTile(tile);
            }
        }

            

        
    }

    private void DrawTile(WallTileSettings tile)
    {

        using (var h0 = new EditorGUILayout.HorizontalScope())
        {
            using (var v0 = new EditorGUILayout.VerticalScope(GUILayout.Width(30)))
            {
                GUI.enabled = !pointTogglesLocked;
                tile.pointToggles = ToggleTilePoint(tile, 0);
                tile.pointToggles = ToggleTilePoint(tile, 3);
                tile.pointToggles = ToggleTilePoint(tile, 6);
                GUI.enabled = true;

            }
            using (var v1 = new EditorGUILayout.VerticalScope(GUILayout.Width(30)))
            {
                GUI.enabled = !pointTogglesLocked;
                tile.pointToggles = ToggleTilePoint(tile, 1);
                tile.pointToggles = ToggleTilePoint(tile, 4);
                tile.pointToggles = ToggleTilePoint(tile, 7);
                GUI.enabled = true;
            }
            using (var v2 = new EditorGUILayout.VerticalScope(GUILayout.Width(30)))
            {
                GUI.enabled = !pointTogglesLocked;
                tile.pointToggles = ToggleTilePoint(tile, 2);
                tile.pointToggles = ToggleTilePoint(tile, 5);
                tile.pointToggles = ToggleTilePoint(tile, 8);
                GUI.enabled = true;
            }
            using (var v3 = new EditorGUILayout.VerticalScope(GUILayout.Width(30)))
            {
                Texture2D croppedTexture = GetTextureOfTile(tile);
                GUILayout.Box(croppedTexture, GUILayout.Width(64), GUILayout.Height(64));
            }
            using (var v2 = new EditorGUILayout.VerticalScope(GUILayout.Width(60)))
            {
                tile.isPocket = EditorGUILayout.Toggle("Is Pocket", tile.isPocket);
            }
        }
        EditorGUILayout.Separator();
    }

    public static Texture2D GetTextureOfTile(WallTileSettings tile)
    {
        Texture2D tex = tile.tile.sprite.texture;
        Rect cropRect = tile.tile.sprite.rect;
        Color[] colors = tex.GetPixels((int)cropRect.x, (int)cropRect.y, (int)cropRect.width, (int)cropRect.height);
        Texture2D croppedTexture = new Texture2D((int)cropRect.width, (int)cropRect.height);
        croppedTexture.SetPixels(colors);
        croppedTexture.Apply(false);
        return croppedTexture;
    }

    private int ToggleTilePoint(WallTileSettings tile, int pos)
    {
        int[] powers = new int[] { 1, 2, 4, 8, 16, 32, 64, 128, 256 };

        int current = tile.pointToggles & powers[pos];
        bool currentToggle = Convert.ToBoolean(current);

        bool newToggle = EditorGUILayout.Toggle(currentToggle);
        
        return (newToggle == currentToggle) ? tile.pointToggles : tile.pointToggles ^ powers[pos];
    }

}
