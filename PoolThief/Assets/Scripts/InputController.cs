﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputController : MonoBehaviour
{
    public float force;
    public BallController ball;
    [SerializeField] LineRenderer line;
    [SerializeField] PhantomController phantom;
    public LayerMask phantomRaycastTarget;


    public Vector3 mousePosition;
    public Vector3 targetVector;

    public float ballRadius;
    public int rayCount = 1;
    public bool debugCollisions;

    private void Start()
    {
        phantom = FindObjectOfType<PhantomController>();
    }

    private void FixedUpdate()
    {
        if (!ball) return;

        mousePosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        targetVector = MathHelper.ZeroZ(mousePosition - ball.transform.position);
        Vector3 dir = targetVector.normalized;

        

        if (Input.GetMouseButtonDown(0))
        {
            ball.AddForce(dir * force);
            line.enabled = false;
        }
        if (ball.myRigidbody.velocity.sqrMagnitude < 0.001f)
        {
            line.enabled = true;
        }

        CastPhantom();
    }

    private void CastAllPhantoms()
    {

    }

    private void CastPhantom()
    {
        if (ball.myRigidbody.velocity.sqrMagnitude > 0) {

            phantom.SetRendering(false);
            return;
        }
        
        RaycastHit hit;
        Ray ray = new Ray(ball.transform.position, targetVector);
        bool getHit = Physics.Raycast(
            ball.transform.position,
            targetVector, 
            out hit,
            100f,
            phantomRaycastTarget,
            QueryTriggerInteraction.Ignore
            );
        
        if (getHit)
        {
            phantom.SetRendering(true);
            phantom.transform.position = hit.point;
            line.SetPosition(0, ball.transform.position + Vector3.back);
            line.SetPosition(1, hit.point + Vector3.back);

            Vector3 reflection = Vector3.Reflect(targetVector, hit.normal).normalized * 10f;
            line.SetPosition(2, hit.point + Vector3.back + reflection);
        }
        else
        {
            phantom.SetRendering(false);
        }
    }
}
