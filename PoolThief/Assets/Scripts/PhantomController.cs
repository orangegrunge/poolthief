﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PhantomController : MonoBehaviour
{
    MeshRenderer meshRenderer;

    private void Start()
    {
        meshRenderer = GetComponent<MeshRenderer>();
    }


    public void SetRendering(bool enable)
    {
        meshRenderer.enabled = enable;
    }
    
    

}
