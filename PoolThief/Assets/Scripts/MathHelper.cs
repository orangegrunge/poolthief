﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class MathHelper 
{
    public static Vector3 ZeroY(Vector3 v)
    {
        return new Vector3(v.x, 0, v.z);
    }

    public static Vector3 ZeroZ(Vector3 v)
    {
        return new Vector3(v.x, v.y, 0);
    }

    public static bool CheckIndexInBounds(int w, int h, int x, int y)
    {
        return !(x < 0 || x > w-1 || y < 0 || y > h-1);
    } 

    public static bool CheckPointInsidePolygon(List<Vector3> polygon, Vector3 point)
    {
        Vector3 rayDir = new Vector3(1f, 0, 100f);
        int counter = 0;

        for (int i = 0; i < polygon.Count; i++)
        {
            int prev = (i == 0) ? polygon.Count - 1 : i - 1;
            if (Math3d.AreLineSegmentsCrossing(point, point+rayDir, polygon[prev], polygon[i])) counter++;
        }

        return counter % 2 == 1;
    }

    public static Vector3 RoundedVector(Vector3 vector)
    {
        return new Vector3(
            Mathf.Round(vector.x),
            Mathf.Round(vector.y),
            Mathf.Round(vector.z)
            );
    }
}
