﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEditor;
using System;
using UnityEngine.Tilemaps;

[System.Serializable]
public class SimplifiedTileSettings
{
    public int t = 0;
    public int w = 0;
    public int p = 0;
    public int b = 0;
    public int x = 0;
    public int y = 0;

    public SimplifiedTileSettings(WallTileSettingsPos settings)
    {
        t = settings.settings.pointToggles;
        w = settings.settings.isWall ? 1 : 0;
        p = settings.settings.isPocket ? 1 : 0;
        b = settings.settings.isBall ? 1 : 0;
        x = settings.pos.x;
        y = settings.pos.y;
    }
}

[System.Serializable]
class LevelSettings
{
    public string levelName;
    public List<SimplifiedTileSettings> levelMap;

    public LevelSettings(string lvlName, List<SimplifiedTileSettings> simplifiedTileSettings)
    {
        levelName = lvlName;
        levelMap = simplifiedTileSettings;
    }
}

[System.Serializable]
class LevelCollection
{
    public List<LevelSettings> levels = new List<LevelSettings>();
}

[System.Serializable]
class LevelPreview
{
    public string levelName;
    public Texture2D texture;

    public LevelPreview(LevelSettings settings,  WallHolder wallHolder)
    {
        levelName = settings.levelName;
        texture = new Texture2D(120, 96);
        

        foreach (SimplifiedTileSettings sts in settings.levelMap)
        {
            WallTileSettings tile = new WallTileSettingsPos(sts, wallHolder).settings;
            Texture2D tileTex = WallHolderEditor.GetTextureOfTile(tile);
            
            int x = (sts.x + 15) * 4;
            int y = (sts.y + 12) * 4;

            for (int i = 0; i < 4; i++)
            {
                for (int j = 0; j < 4; j++)
                {
                    texture.SetPixel(x + j, y + i,
                        tileTex.GetPixelBilinear((1 + 2 * j) / 8f, (1 + 2 * i) / 8f));
                }
            }
        }
        texture.Apply();
    }

    Color GetColorByToggles(int pos, SimplifiedTileSettings sts)
    {
        int[] powers = new int[] { 1, 2, 4, 8, 16, 32, 64, 128, 256 };

        return Convert.ToBoolean(sts.t & powers[pos]) ? Color.black : Color.gray;
    }
}


class LevelEditor: MonoBehaviour
{
    public static List<LevelPreview> levelPreviews = new List<LevelPreview>();

    public static void UpdateWalls()
    {
        WallManager wallManager = FindObjectOfType<WallManager>();
        wallManager.UpdateAllFromTileMap();
        wallManager.ShowCombined();
    }

    public static void ForceDeleteWalls()
    {
        WallManager wallManager = FindObjectOfType<WallManager>();
        wallManager.ForceDeleteWalls();
        wallManager.HideCombined();
    }

    public static void SaveLevel()
    {
        UpdateWalls();
        WallManager wallManager = FindObjectOfType<WallManager>();
        List<WallTileSettingsPos> wallTileSettings = wallManager.GetCurrentSettings();
        List<SimplifiedTileSettings> levelMapSettings = new List<SimplifiedTileSettings>();
        foreach (WallTileSettingsPos ws in wallTileSettings)
        {
            levelMapSettings.Add(new SimplifiedTileSettings(ws));
        }

        string lvlName = EditorPrefs.GetString("LevelEditor_LevelName");

        LevelSettings levelSettings = new LevelSettings(lvlName, levelMapSettings);

        WriteLevel(levelSettings);
    }

    public static void RemoveLevel(string name)
    {
        LevelCollection levelCollection = ReadLevels();
        if (levelCollection == null) levelCollection = new LevelCollection();

        if (levelCollection.levels.Exists(x => x.levelName == name))
        {
            int index = levelCollection.levels.FindIndex(x => x.levelName == name);
            levelCollection.levels.RemoveAt(index);

            UpdateLevelPreviews(levelCollection);
        }
        else
        {
            Debug.LogWarning("LevelEditor: No level with name " + name + " to remove!");
        }

        string json = JsonUtility.ToJson(levelCollection);
        File.WriteAllText("Assets/Configs/levels.json", json);
    }

    internal static void LoadLevel(string levelName)
    {
        LevelCollection levelCollection = ReadLevels();
        if (levelCollection == null)
        {
            Debug.LogWarning("LevelEditor: No saved levels!");
            return;
        }

        LevelSettings level;

        if (levelCollection.levels.Exists(x => x.levelName == levelName))
        {
            level = levelCollection.levels.Find(x => x.levelName == levelName);
            ForceDeleteWalls();
            WallManager wallManager = FindObjectOfType<WallManager>();
            wallManager.UpdateAllFromSettings(level.levelMap);

            UpdateLevelPreviews(levelCollection);
        }
        else
        {
            Debug.LogWarning("LevelEditor: No saved level with name " + levelName + "!");
            return;
        }
    }

    private static void WriteLevel(LevelSettings levelSettings)
    {
        LevelCollection levelCollection = ReadLevels();
        if (levelCollection == null) levelCollection = new LevelCollection();

        if (levelCollection.levels.Exists(x => x.levelName == levelSettings.levelName)) {
            int index = levelCollection.levels.FindIndex(x => x.levelName == levelSettings.levelName);
            levelCollection.levels[index].levelMap = levelSettings.levelMap;
        }
        else
        {
            levelCollection.levels.Add(levelSettings);
        }

        string json = JsonUtility.ToJson(levelCollection);
        File.WriteAllText("Assets/Configs/levels.json", json);

        UpdateLevelPreviews(levelCollection);
    }

    private static LevelCollection ReadLevels()
    {
        string json = File.ReadAllText("Assets/Configs/levels.json");
        return JsonUtility.FromJson<LevelCollection>(json);
    }

    private static void UpdateLevelPreviews(LevelCollection collection) {
        WallHolder wallHolder = FindObjectOfType<WallHolder>();
        levelPreviews = new List<LevelPreview>();

        collection.levels.ForEach(x => {
            levelPreviews.Add(new LevelPreview(x, wallHolder));
        });

    }

    public static void UpdateLevelPreviews()
    {
        LevelCollection levelCollection = ReadLevels();
        if (levelCollection == null) levelCollection = new LevelCollection();
        UpdateLevelPreviews(levelCollection);
    }
}
