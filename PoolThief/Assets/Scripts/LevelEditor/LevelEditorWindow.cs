﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using System;

public class LevelEditorWindow : EditorWindow
{
    static Vector2 scrollPos;



    [MenuItem("Pool/LevelEditor")]
    public static void ShowWindow()
    {
        EditorWindow.GetWindow(typeof(LevelEditorWindow));
        LevelEditor.UpdateLevelPreviews();
    }

    private void OnGUI()
    {
        using (var h0 = new GUILayout.HorizontalScope())
        {
            EditModeButton();
            BuildModeButton();
        }
        GUILayout.Space(5);
        UpdatePrewiewsButton();

        GUILayout.Space(5);
        SaveLevelButton();
        LevelNameText();
        LevelsList();
    }

    private void UpdatePrewiewsButton()
    {
        if (GUILayout.Button("Update previews")) LevelEditor.UpdateLevelPreviews();
    }

    private static void EditModeButton()
    {
        if (EditorPrefs.GetBool("LevelEditor_EditMode"))
        {
            GUI.enabled = false;
        }
        if (GUILayout.Button("Edit Mode"))
        {
            LevelEditor.ForceDeleteWalls();
            EditorPrefs.SetBool("LevelEditor_EditMode", true);
            EditorPrefs.SetBool("LevelEditor_BuildMode", false);
        }
        GUI.enabled = true;
    }

    private static void BuildModeButton()
    {
        if (EditorPrefs.GetBool("LevelEditor_BuildMode"))
        {
            GUI.enabled = false;
        }
        if (GUILayout.Button("Build Mode"))
        {
            LevelEditor.UpdateWalls();
            EditorPrefs.SetBool("LevelEditor_EditMode", false);
            EditorPrefs.SetBool("LevelEditor_BuildMode", true);
        }
        GUI.enabled = true;
    }

    private static void SaveLevelButton()
    {
        if (GUILayout.Button("Save Level"))
        {
            LevelEditor.SaveLevel();
            EditorPrefs.SetBool("LevelEditor_EditMode", false);
            EditorPrefs.SetBool("LevelEditor_BuildMode", true);
        }
    }

    private static void LevelNameText()
    {
        EditorPrefs.SetString("LevelEditor_LevelName", 
            GUILayout.TextField(EditorPrefs.GetString("LevelEditor_LevelName")));
    }

    private static void LoadLevelButton(string name)
    {
        if (GUILayout.Button("Load Level"))
        {
            LevelEditor.LoadLevel(name);
            LevelEditor.UpdateWalls();
            EditorPrefs.SetBool("LevelEditor_EditMode", false);
            EditorPrefs.SetBool("LevelEditor_BuildMode", true);
            EditorPrefs.SetString("LevelEditor_LevelName", name);
        }
    }

    private static void RemoveLevelButton(string levelName)
    {
        if (GUILayout.Button("Remove Level"))
        {
            LevelEditor.RemoveLevel(levelName);
        }
    }

    private static void LevelsList()
    {
        using (var s0 = new EditorGUILayout.ScrollViewScope(scrollPos, GUILayout.Height(500)))
        {
            scrollPos = s0.scrollPosition;

            foreach (LevelPreview lp in LevelEditor.levelPreviews)
            {
                LevelPreview(lp);
            }
        }
         
    }

    private static void LevelPreview(LevelPreview lp)
    {
        using (var h0 = new EditorGUILayout.HorizontalScope())
        {
            using (var v0 = new EditorGUILayout.VerticalScope())
            {
                using (var h1 = new EditorGUILayout.HorizontalScope())
                {
                    GUILayout.Label("Name: ", EditorStyles.boldLabel);
                    GUILayout.Label(lp.levelName);
                }

                LoadLevelButton(lp.levelName);
                RemoveLevelButton(lp.levelName);
            }
                
            GUILayout.Box(lp.texture);
        }
    }
}
