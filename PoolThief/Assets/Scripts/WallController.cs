﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;


public class WallController : MonoBehaviour
{
    WallTileSettings tileSettings;

    public GameObject linePrefab;
    public GameObject filletPrefab;
    public GameObject cornerPrefab;
    public GameObject pocketCornerPrefab;

    List<GameObject> walls = new List<GameObject>();
    List<GameObject> eqdLines = new List<GameObject>();
    List<GameObject> corners = new List<GameObject>();
    List<GameObject> floor = new List<GameObject>();

    public float ballRadius;

    public void Delete()
    {
        for (int i = 0; i < walls.Count; i++) DestroyImmediate(walls[i]);
        for (int i = 0; i < eqdLines.Count; i++) DestroyImmediate(eqdLines[i]);
        for (int i = 0; i < corners.Count; i++) DestroyImmediate(corners[i]);
        DestroyImmediate(gameObject);
    }

    public void MakeAllWalls(WallTileSettings settings)
    {
        tileSettings = settings;

        if (settings.isPocket)
        {
            MakeAllPocketWalls();
        }
        else if (tileSettings.pointToggles == (int)Math.Pow(2f, 4f))
        {
            MakeCeiling();
        }
        else if (tileSettings.pointToggles == 0)
        {
            MakeFloor();
        }
        else
        {
            MakeAllBoardWalls();
        }

        transform.parent = GameObject.FindGameObjectWithTag("WallControllers").transform;
    }

    private void MakeFloor()
    {
        GameObject wall = Instantiate(linePrefab);
        wall.transform.eulerAngles = Vector3.zero;
        wall.transform.position = transform.position + Vector3.forward * 0.5f;

        wall.transform.parent = gameObject.transform;
        floor.Add(wall);
    }

    private void MakeAllPocketWalls()
    {
        if (GetToggle(1)) { MakePocketWall(Vector2.up, 270, 1, false); }
        if (GetToggle(3)) { MakePocketWall(Vector2.left, 0, 1, false); }
        if (GetToggle(7)) { MakePocketWall(Vector2.down, 90, 1, false); }
        if (GetToggle(5)) { MakePocketWall(Vector2.right, 180, 1, false); }

        if (GetToggle(0) & GetToggle(8) & GetToggle(4))
        {
            if (GetToggle(2) & !GetToggle(6))
            {
                MakePocketCorner(270);
            }
            if (GetToggle(6) & !GetToggle(2) & GetToggle(4))
            {
                MakePocketCorner(90);
            }
        }

        if (GetToggle(2) & GetToggle(6) & GetToggle(4))
        {
            if (GetToggle(0) & !GetToggle(8))
            {
                MakePocketCorner(0);
            }
            if (GetToggle(8) & !GetToggle(0))
            {
                MakePocketCorner(180);
            }
        }
    }

    private void MakeAllBoardWalls()
    {
        if (GetToggle(1)) { MakeWall(Vector2.up, 90, 1, false); }
        if (GetToggle(3)) { MakeWall(Vector2.left, 180, 1, false); }
        if (GetToggle(7)) { MakeWall(Vector2.down, 270, 1, false); }
        if (GetToggle(5)) { MakeWall(Vector2.right, 0, 1, false); }

        if (GetToggle(0) & GetToggle(8) & GetToggle(4))
        {
            if (GetToggle(2) & !GetToggle(6))
            {
                MakeBoardFillet(90, new Vector2(-1, -1));
            }
            if (GetToggle(6) & !GetToggle(2) & GetToggle(4))
            {
                MakeBoardFillet(270, new Vector2(1, 1));
            }
        }

        else if (GetToggle(2) & GetToggle(6) & GetToggle(4))
        {
            if (GetToggle(0) & !GetToggle(8))
            {
                MakeBoardFillet(180, new Vector2(1, -1));
            }
            if (GetToggle(8) & !GetToggle(0))
            {
                MakeBoardFillet(0, new Vector2(-1, 1));
            }
        }
        else MakeCeiling();

        if (GetToggle(0)) { MakeCorner(new Vector2(-1f, 1f)); }
        if (GetToggle(2)) { MakeCorner(new Vector2(1f, 1f)); }
        if (GetToggle(6)) { MakeCorner(new Vector2(-1f, -1f)); }
        if (GetToggle(8)) { MakeCorner(new Vector2(1f, -1f)); }
    }

    internal List<MeshFilter> GetMeshFilters(bool isFloor)
    {
        List<MeshFilter> meshFilters = new List<MeshFilter>();

        List<GameObject> source = isFloor ? floor : walls;
        
        foreach (GameObject wall in source)
        {
            meshFilters.Add(wall.GetComponent<MeshFilter>());
        }
        return meshFilters;
    }

    bool GetToggle(int p)
    {
        int[] powers = new int[] { 1, 2, 4, 8, 16, 32, 64, 128, 256 };
        return Convert.ToBoolean(tileSettings.pointToggles & powers[p]);
    }

    void MakeWall(Vector2 dir, int rotation, float scale, bool noOffset)
    {
        GameObject wall = Instantiate(linePrefab);
        wall.transform.eulerAngles = new Vector3(rotation, -90, -90);
        wall.transform.localScale = new Vector3(scale, 1, 1);
        if (noOffset) wall.transform.position = (Vector2)transform.position;
        else wall.transform.position = dir.normalized * 0.5f + (Vector2)transform.position;

        wall.transform.parent = gameObject.transform;
        walls.Add(wall);

        GameObject eqd = Instantiate(linePrefab);
        eqd.transform.eulerAngles = new Vector3(rotation, -90, -90);
        eqd.transform.localScale = new Vector3(scale, 1, 1);
        if (noOffset) eqd.transform.position = (Vector2)transform.position + dir.normalized * ballRadius;
        else eqd.transform.position = dir.normalized * 0.5f + (Vector2)transform.position + dir.normalized * ballRadius;

        eqd.transform.parent = gameObject.transform;
        eqd.layer = LayerMask.NameToLayer("EQD");
        eqdLines.Add(eqd);

    }

    private void MakeCeiling()
    {
        GameObject ceiling = Instantiate(linePrefab);
        ceiling.transform.eulerAngles = Vector3.zero;
        ceiling.transform.position = transform.position - Vector3.forward * 0.5f;

        ceiling.transform.parent = gameObject.transform;
        walls.Add(ceiling);
    }

    void MakeCorner(Vector2 dir)
    {
        GameObject corner = Instantiate(cornerPrefab);
        corner.transform.localScale = new Vector3(ballRadius * 2f, 1, ballRadius * 2f);
        corner.transform.position = dir.normalized * Mathf.Sqrt(2) * 0.5f + (Vector2)transform.position;
        corner.transform.parent = gameObject.transform;
        corner.layer = LayerMask.NameToLayer("EQD");

        corners.Add(corner);

    }

    void MakePocketCorner(int rotation)
    {
        GameObject wall = Instantiate(pocketCornerPrefab);
        wall.transform.eulerAngles = new Vector3(0, 0, rotation);
        wall.transform.position = transform.position + Vector3.forward * 1.5f;
        wall.transform.parent = gameObject.transform;
        floor.Add(wall);
    }

    void MakePocketWall(Vector2 dir, int rotation, float scale, bool noOffset)
    {
        GameObject wall = Instantiate(linePrefab);
        wall.transform.eulerAngles = new Vector3(rotation, -90, -90);
        wall.transform.localScale = new Vector3(scale, 2, 1);
        wall.transform.position = (Vector3)dir.normalized * 0.5f + transform.position + Vector3.forward * 1.5f;

        wall.transform.parent = gameObject.transform;
        floor.Add(wall);
    }

    void MakeBoardFillet(int rotation, Vector2 dir)
    {
        GameObject wall = Instantiate(filletPrefab);
        wall.transform.eulerAngles = new Vector3(0, 0, rotation);
        wall.transform.position = transform.position;
        wall.transform.parent = gameObject.transform;
        walls.Add(wall);

        GameObject eqd = Instantiate(linePrefab);
        eqd.transform.eulerAngles = new Vector3(rotation + 135, -90, -90);
        eqd.transform.localScale = new Vector3(Mathf.Sqrt(2), 1, 1);
        eqd.transform.position = (Vector2)transform.position + dir.normalized * ballRadius;
        eqd.transform.parent = gameObject.transform;
        eqd.layer = LayerMask.NameToLayer("EQD");
        eqdLines.Add(eqd);

        MakeFloor();
    }

    public void ToggleEQDVisible(bool state)
    {
        foreach (GameObject eqd in eqdLines)
        {
            MeshRenderer mr = eqd.GetComponent<MeshRenderer>();
            mr.enabled = state;
        }

        foreach (GameObject corner in corners)
        {
            MeshRenderer mr = corner.GetComponent<MeshRenderer>();
            mr.enabled = state;

        }
    }

    public void ToggleWallsVisible(bool state)
    {
        foreach (GameObject wall in walls)
        {
            MeshRenderer mr = wall.GetComponent<MeshRenderer>();
            mr.enabled = state;

        }
    }
}
