﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;


public class WallTileSettingsPos
{
    public WallTileSettings settings;
    public Vector2Int pos;

    public WallTileSettingsPos() { }

    public WallTileSettingsPos(SimplifiedTileSettings sts, WallHolder wallHolder)
    {
        settings = new WallTileSettings();
        settings.pointToggles = sts.t;
        settings.isBall = sts.b == 1 ? true : false;
        settings.isPocket = sts.p == 1 ? true : false;
        settings.isWall = sts.w == 1 ? true : false;

        settings.tile = wallHolder.GetTileBySettings(settings);

        pos = new Vector2Int(sts.x, sts.y);
    }
}

public class WallManager : MonoBehaviour
{
    List<WallController> wallControllers = new List<WallController>();
    List<WallTileSettingsPos> wallSettings = new List<WallTileSettingsPos>();

    public GameObject wallControllerPrefab;

    public Tilemap tilemap;
    WallHolder wallHolder;
    public float ballRadius;

    bool eqdIsVisible = true;
    bool wallsIsVisible = true;

    public TableMeshController table; 
    public TableMeshController walls; 

    [ContextMenu("UPDATE ALL")]
    public void UpdateAllFromTileMap()
    {
        ForceDeleteWalls();
        SetSettingsFromTileMap();


        foreach (WallTileSettingsPos settingsPos in wallSettings)
        {
            InstantiateWallController(settingsPos);
        }

        CombineMeshes(wallControllers, table, true);
        CombineMeshes(wallControllers, walls, false);
    }

    private void InstantiateWallController(WallTileSettingsPos settingsPos)
    {
        GameObject wall = Instantiate(wallControllerPrefab);
        Vector3Int pos = new Vector3Int(settingsPos.pos.x, settingsPos.pos.y, 0);
        wall.transform.position = (Vector3)pos + new Vector3(0.5f, 0.5f,0);
        WallController controller = wall.GetComponent<WallController>();
        controller.ballRadius = ballRadius;
        if (settingsPos.settings != null)
        {
            controller.MakeAllWalls(settingsPos.settings);
        }
        controller.ToggleEQDVisible(false);
        controller.ToggleWallsVisible(false);
        wallControllers.Add(controller);
    }

    public void UpdateAllFromSettings(List<SimplifiedTileSettings> sts)
    {
        ForceDeleteWalls();
        SetSettingsFromSimplified(sts);
        UpdateTileMapFromSettings();

        foreach (WallTileSettingsPos settingsPos in wallSettings)
        {
            InstantiateWallController(settingsPos);
        }

        CombineMeshes(wallControllers, table, true);
        CombineMeshes(wallControllers, walls, false);
    }

    public void SetSettingsFromTileMap()
    {
        wallSettings = new List<WallTileSettingsPos>();
        for (int y = -20; y < 20; y++)
        {
            for (int x = -20; x < 20; x++)
            {
                TileBase tile = tilemap.GetTile(new Vector3Int(x, y, 0));
                if (tile)
                {
                    WallTileSettingsPos settings = new WallTileSettingsPos();
                    settings.settings = wallHolder.GetByName(tile.name);
                    settings.pos = new Vector2Int(x, y);
                    wallSettings.Add(settings);
                }
            }
        }
    }

    public void UpdateTileMapFromSettings()
    {
        tilemap.ClearAllTiles();

        foreach (WallTileSettingsPos settingsPos in wallSettings)
        {
            tilemap.SetTile((Vector3Int)settingsPos.pos, settingsPos.settings.tile);
        }

    }

    public void SetSettingsFromSimplified(List<SimplifiedTileSettings> simplifiedTileSettings)
    {
        wallSettings = new List<WallTileSettingsPos>();
        foreach (SimplifiedTileSettings sts in simplifiedTileSettings)
        {
            wallSettings.Add(new WallTileSettingsPos(sts, wallHolder));
        }

    }

    public List<WallTileSettingsPos> GetCurrentSettings()
    {
        return wallSettings;
    }

    internal void HideCombined()
    {
        table.gameObject.SetActive(false);
        walls.gameObject.SetActive(false);
        tilemap.GetComponent<TilemapRenderer>().enabled = true;
    }

    internal void ShowCombined()
    {
        table.gameObject.SetActive(true);
        walls.gameObject.SetActive(true);
        tilemap.GetComponent<TilemapRenderer>().enabled = false;
    }

    private void CombineMeshes(List<WallController> wc, TableMeshController tMC, bool isFloor)
    {
        List<MeshFilter> meshFilters = new List<MeshFilter>();

        foreach (WallController controller in wc)
        {
            List<MeshFilter> wallMeshFilters = controller.GetMeshFilters(isFloor);
            meshFilters.AddRange(wallMeshFilters);
        }

        CombineInstance[] combine = new CombineInstance[meshFilters.Count];

        int i = 0;
        while (i < meshFilters.Count)
        {
            combine[i].mesh = meshFilters[i].sharedMesh;
            combine[i].transform = meshFilters[i].transform.localToWorldMatrix;
            meshFilters[i].gameObject.SetActive(false);

            i++;
        }
        tMC.GetComponent<MeshFilter>().sharedMesh = new Mesh();
        tMC.GetComponent<MeshFilter>().sharedMesh.CombineMeshes(combine);
        tMC.GetComponent<MeshCollider>().sharedMesh = tMC.GetComponent<MeshFilter>().sharedMesh;
        tMC.gameObject.SetActive(true);
    }


    void UpdateOnStart() {
        UpdateAllFromTileMap();
        ForceDeleteWalls();
    }

    [ContextMenu("Force Delete Walls")]
    public void ForceDeleteWalls()
    {
        DeleteAll();
        WallController[] controllers = FindObjectsOfType<WallController>();
        foreach (WallController controller in controllers)
        {
            controller.Delete();
        }
        table.GetComponent<MeshFilter>().sharedMesh = null;
        walls.GetComponent<MeshFilter>().sharedMesh = null;
    }

    [ContextMenu("DELETE ALL")]
    public void DeleteAll()
    {
        if (!wallHolder) wallHolder = FindObjectOfType<WallHolder>();

        for (int i = 0; i < wallControllers.Count; i++)
        {
            if (wallControllers[i]) wallControllers[i].Delete();
        }

        wallControllers = new List<WallController>();
    }

    public void ToggleEQDVisible()
    {
        eqdIsVisible = !eqdIsVisible;
        foreach (WallController controller in wallControllers)
        {
            controller.ToggleEQDVisible(eqdIsVisible);
        }

        WallController[] controllers = FindObjectsOfType<WallController>();
        foreach (WallController controller in controllers)
        {
            controller.ToggleEQDVisible(eqdIsVisible);
        }

    }

    public void ToggleWallsVisible()
    {
        wallsIsVisible = !wallsIsVisible;
        foreach (WallController controller in wallControllers)
        {
            controller.ToggleWallsVisible(wallsIsVisible);
        }

        WallController[] controllers = FindObjectsOfType<WallController>();
        foreach (WallController controller in controllers)
        {
            controller.ToggleWallsVisible(wallsIsVisible);
        }
    }
}
