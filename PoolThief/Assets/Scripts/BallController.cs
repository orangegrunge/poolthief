﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BallController : MonoBehaviour
{
    public bool isCueBall = false;
    public Rigidbody myRigidbody;
    public DebugCollision debugCollisionPrefab;

    public Collider otherBody;
    public LineRenderer lineRenderer;

    public InputController inputController;
    public GameStateController stateController;

    internal void Remove()
    {
        stateController.RemoveBall(this);


        Destroy(gameObject);
    }

    private void Start()
    {
        myRigidbody = GetComponent<Rigidbody>();
        lineRenderer = GetComponent<LineRenderer>();
        inputController = FindObjectOfType<InputController>();
        stateController = FindObjectOfType<GameStateController>();
        stateController.InitBall(this);
    }

    private void FixedUpdate()
    {
        if (myRigidbody.velocity.z < 0)
        {
            myRigidbody.velocity = (Vector2)myRigidbody.velocity;
        }
        if (myRigidbody.velocity.sqrMagnitude < 0.1f)
        {
            myRigidbody.velocity = Vector3.zero;
            myRigidbody.angularVelocity = Vector3.zero;
        }
        
        // lineRenderer.SetPositions(new Vector3[] { transform.position + Vector3.back, transform.position + Vector3.back + myRigidbody.velocity});
    }

    public void AddForce(Vector3 force)
    {

        myRigidbody.AddForce(force, ForceMode.Impulse);
    }

    public void OnCollisionEnter(Collision collision)
    {
        //if (inputController.debugCollisions) DrawDebugCollision(collision);

    }

    private void DrawDebugCollision(Collision collision)
    {
        ContactPoint[] contacts = new ContactPoint[1];

        Debug.Log(collision.contactCount);

        int contactCount = collision.GetContacts(contacts);
        Vector3 reflectedVelocity = Vector3.Reflect(myRigidbody.velocity, contacts[0].normal) / 3f;

        DebugCollision dc = Instantiate(debugCollisionPrefab);
        dc.Draw(contacts[0].point, myRigidbody.velocity, contacts[0].normal, reflectedVelocity);
    }
}
