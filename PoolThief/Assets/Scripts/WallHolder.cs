﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;

[System.Serializable]
public class WallTileSettings
{
    public int pointToggles = 0;

    public bool isWall = false;
    public bool isPocket = false;
    public bool isBall = false;

    public Tile tile;

    public WallTileSettings() { }

    public WallTileSettings(Tile t)
    {
        tile = t;
    }
}

public class WallHolder : MonoBehaviour
{
    [SerializeField] public List<WallTileSettings> tiles = new List<WallTileSettings>();
    WallHolderReciever wallHolderReciever;


    public void UpdateTileList()
    {
        if (!wallHolderReciever) wallHolderReciever = GetComponent<WallHolderReciever>();

        List<WallTileSettings> newTiles = new List<WallTileSettings>();

        foreach (Tile tile in wallHolderReciever.tiles)
        {
            if (!tile) continue;

            bool added = false;
            foreach (WallTileSettings settings in tiles)
            {
                if (tile.name == settings.tile.name)
                {
                    newTiles.Add(settings);
                    added = true;
                    break;
                }
            }

            if (!added)
            {
                newTiles.Add(new WallTileSettings(tile));
            }

        }

        tiles = newTiles;
    }

    internal Tile GetTileBySettings(WallTileSettings search)
    {
        foreach (WallTileSettings settings in tiles)
        {
            if (search.pointToggles == settings.pointToggles 
                && search.isBall == settings.isBall
                && search.isPocket == settings.isPocket
                && search.isWall == settings.isWall)
            {
                return settings.tile;
            }
        }

        return null;
    }

    public WallTileSettings GetByName(string name)
    {
        foreach (WallTileSettings settings in tiles)
        {
            if (name == settings.tile.name)
            {
                return settings;
            }
        }

        return null;
    }
}


