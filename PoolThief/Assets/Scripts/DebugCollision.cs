﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DebugCollision : MonoBehaviour
{
    public LineRenderer IN;
    public LineRenderer OUT;
    public LineRenderer NORMAL;

    public void Draw(Vector3 point, Vector3 velocity, Vector3 norm, Vector3 reflection)
    {
        Vector3[] input = new Vector3[] {
            point + Vector3.back,  point + velocity.normalized * 2f  + Vector3.back
        };
        Vector3[] output = new Vector3[] {
            point + Vector3.back, point + norm * 2f + Vector3.back
        };
        Vector3[] normal = new Vector3[] {
            point + Vector3.back, point - reflection.normalized * 2f + Vector3.back
        };

        IN.SetPositions(input);
        OUT.SetPositions(output);
        NORMAL.SetPositions(normal);
    }

}
