﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DeathCollider : MonoBehaviour
{

    private void Start()
    {
        
    }


    private void OnTriggerEnter(Collider other)
    {
        BallController ballController = other.GetComponent<BallController>();
        if (ballController)
        {
            ballController.Remove();
        }
    }
}
